"""MxShop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
# from django.contrib import admin
import xadmin
from django.views.static import serve
from rest_framework.documentation import include_docs_urls
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
from rest_framework_jwt.views import obtain_jwt_token
from django.views.generic import TemplateView

router = DefaultRouter()

from MxShop.settings import MEDIA_ROOT
# from goods.views_base import GoodListView
from goods.views import GoodsListViewSet, CategoryViewSet, HotSearchsViewSet, BannerViewSet, IndexCategoryViewSet
from users.views import SmsCodeViewSet, UserViewSet
from user_operation.views import UserFavViewSet, LeavingMessageViewSet, AddressViewSet
from trade.views import ShoppingCartViewSet, OrderViewSet, AliPayView

# 配置 goods 的 url
router.register(r'goods', GoodsListViewSet, base_name="goods")  # 商品
router.register(r'categorys', CategoryViewSet, base_name="categorys")  # 商品分类
router.register(r'codes', SmsCodeViewSet, base_name="codes")  # 验证码
router.register(r'users', UserViewSet, base_name="users")  # 用户
router.register(r'userfavs', UserFavViewSet, base_name="userfavs")  # 收藏
router.register(r'messages', LeavingMessageViewSet, base_name="messages")  # 留言
router.register(r'address', AddressViewSet, base_name="address")  # 收货地址
router.register(r'shopcats', ShoppingCartViewSet, base_name="shopcats")  # 购物车
router.register(r'orders', OrderViewSet, base_name="orders")  # 订单相关
router.register(r'hotsearchs', HotSearchsViewSet, base_name="hotsearchs")  # 热搜
router.register(r'banners', BannerViewSet, base_name="banners")  # 轮播图
router.register(r'indexgoods', IndexCategoryViewSet, base_name="indexgoods")  # 首页商品系列数据

# goods_list = GoodsListViewSet.as_view({
#     'get': 'list'
# })

urlpatterns = [
    url(r'^index/', TemplateView.as_view(template_name="index.html"), name="index"),
    url(r'^xadmin/', xadmin.site.urls),
    url(r'^media/(?P<path>.*)$', serve, {"document_root": MEDIA_ROOT}),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # drf 自带的 token 认证模式
    url(r'^api-token-auth/', views.obtain_auth_token),
    # jwt 的认证接口
    url(r'^login/$', obtain_jwt_token),

    # 商品列表页
    # url(r'goods/$', GoodsListView.as_view(), name="goods-list"),
    # url(r'goods/$', goods_list, name="goods-list"),
    url(r'^', include(router.urls)),

    url(r'docs/', include_docs_urls(title="文档功能")),

    url(r'^alipay/return/', AliPayView.as_view(), name="alipay"),

    # 第三方登录
    url('', include('social_django.urls', namespace='social')),
]
