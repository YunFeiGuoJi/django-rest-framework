# -*- coding: utf-8 -*-
from django.views.generic.base import View
# from django.views.generic import ListView

from .models import Goods


class GoodListView(View):
    def get(self, request):
        """
        通过 django 的 view 实现商品列表页
        :param request:
        :return:
        """
        json_list = []
        goods = Goods.objects.all()[:10]
        # for good in goods:
        #     json_dict = {}
        #     json_dict["name"] = good.name
        #     json_dict["category"] = good.category.name
        #     json_dict["market_price"] = goods.market_price
        #     json_list.append(json_dict)

        # from django.forms.models import model_to_dict
        # for good in goods:
        #     json_dict = model_to_dict(good)
        #     json_list.append(json_dict)

        import json
        from django.core import serializers
        json_data = serializers.serialize("json", goods)
        json_data = json.loads(json_data)
        from django.http import HttpResponse, JsonResponse
        return HttpResponse(json.dumps(json_data), content_type="application/json")

        # from django.http import HttpResponse
        # import json
        # return HttpResponse(json.dumps(json_list), content_type="application/json")

