# -*- coding: utf-8 -*-


def get_auth_url():
    weibo_auth_url = "https://api.weibo.com/oauth2/authorize"
    redirect_url = "http://106.14.156.160:8000/complete/weibo/"
    auth_url = weibo_auth_url + "?client_id={client_id}&redirect_uri={re_url}".format(client_id=2710259933,
                                                                                      re_url=redirect_url)
    print(auth_url)


def get_access_token(code="4c624ebcce5806face8af798ad33f969"):
    access_token_url = "https://api.weibo.com/oauth2/access_token"
    import requests
    re_dict = requests.post(access_token_url, data={
        "client_id": "2710259933",
        "client_secret": "f01a2549b57a7eb8b8cad60b67b105ff",
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": "http://106.14.156.160:8000/complete/weibo/"
    })
    pass

# '{"access_token":"2.00I3jrgCt6y6xC19cdd6a754MXX2FD","remind_in":"157679999","expires_in":157679999,"uid":"2465677512","isRealName":"true"}'


def get_user_info(access_token="", uid=""):
    user_url = "https://api.weibo.com/2/users/show.json?access_token={token}&uid={uid}".format(token=access_token, uid=uid)
    print(user_url)


if __name__ == "__main__":
    # get_auth_url()
    # get_access_token(code="4c624ebcce5806face8af798ad33f969")
    get_user_info(access_token="2.00I3jrgCt6y6xC19cdd6a754MXX2FD", uid="2465677512")
